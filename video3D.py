import argparse
import math
import pathlib
import shutil
import traceback
import typing

from eprempy import eprem
from eprempy import metric
import imageio
import numpy
import matplotlib as mpl
from matplotlib.colors import ListedColormap, BoundaryNorm
from matplotlib.cm import ScalarMappable
import matplotlib.pyplot as plt

from support import interfaces


def main(**user) -> None:
    """Create a GIF video of a physical quantity from an EPREM run."""
    dataset = eprem.dataset(user['source'], user['config'])
    parameters = dataset.parameters
    times = get_times(parameters, user)
    streams = tuple(dataset.streams.values())
    points = tuple(dataset.points.values())
    nshells = parameters['numNodesPerStream']
    verbose = user['verbose']
    tmpdir = pathlib.Path.cwd() / '.video3D-tmp'
    tmpdir.mkdir(parents=True, exist_ok=True)
    pngnames = create_frames(
        times,
        nshells,
        streams,
        points,
        tmpdir,
        user,
    )
    pngpaths = [tmpdir / pngname for pngname in pngnames]
    frames = [imageio.v2.imread(pngpath) for pngpath in pngpaths]
    savename = f"{str(user['quantity']).replace(' ', '-')}.gif"
    savepath = pathlib.Path(user['save_path'] or savename)
    if savepath.is_dir():
        savepath = savepath / savename
    try:
        imageio.mimsave(savepath, frames, fps=user['fps'])
    except Exception as exc:
        raise RuntimeError("Unable to create video") from exc
    if verbose:
        print(f"Saved {savepath}")
    if pngdir := get_pngdir(user, savepath):
        pngdir.mkdir(parents=True, exist_ok=True)
        for pngpath in pngpaths:
            pngpath.rename(pngdir / pngpath.name)
        if verbose:
            print(f"Saved PNGs to {pngdir}")
    shutil.rmtree(tmpdir)


def get_pngdir(user: dict, savepath: pathlib.Path):
    """Get the directory to which to save PNGs, if applicable."""
    if user['save_pngs'] is False:
        return
    if user['save_pngs'] is None:
        return savepath.with_name(savepath.stem + '-pngs')
    return pathlib.Path(user['save_pngs']).expanduser().resolve()


def get_times(
    parameters: eprem.parameter.Interface,
    user: dict,
) -> eprem.physical.Vector:
    """Compute the sequence of times to visualize."""
    time_step = user['time_step']
    start_time = parameters['simStartTime'][0]
    stop_time = parameters['simStopTime'][0]
    dt = float(parameters['tDel'])
    step = interfaces.measure_indexer(time_step)
    if step.isunitless:
        nsteps = int((float(stop_time) - float(start_time)) / dt)
        return eprem.physical.vector(numpy.arange(0, nsteps, int(step)))
    unit = step.unit
    start = float(start_time.withunit(unit))
    stop = float(stop_time.withunit(unit))
    times = numpy.arange(start, stop, float(step))
    return eprem.physical.vector(times, unit=unit)


def create_frames(
    times: eprem.physical.Vector,
    nshells: int,
    streams: typing.Sequence[eprem.Stream],
    points: typing.Sequence[eprem.Point],
    tmpdir: pathlib.Path,
    user: dict,
) -> typing.List[str]:
    """Main time-step loop."""
    ntimes = len(times)
    px = numpy.array([point['x'][0].withunit('au') for point in points])
    py = numpy.array([point['y'][0].withunit('au') for point in points])
    pz = numpy.array([point['z'][0].withunit('au') for point in points])
    quantity = user['quantity']
    name = str(quantity).replace(' ','_')
    indices = get_stream_indices(user)
    unit = user['unit']
    verbose = user['verbose']
    cmin = user['cmin']
    cmax = user['cmax']
    titles = get_time_labels(times)
    pngnames = []
    for step, time in enumerate(times):
        try:
            sx = get_stream_position('x', streams, time, nshells)
            sy = get_stream_position('y', streams, time, nshells)
            sz = get_stream_position('z', streams, time, nshells)
            values = get_stream_values(
                quantity=quantity,
                streams=streams,
                time=time,
                nshells=nshells,
                indices=indices,
                unit=unit,
            )
            vmin = numpy.min(values) if cmin is None else cmin
            vmax = numpy.max(values) if cmax is None else cmax
            create_png(
                ckey=user['colormap'],
                vmin=vmin,
                vmax=vmax,
                log=user['log'],
                sx=sx.reshape(-1),
                sy=sy.reshape(-1),
                sz=sz.reshape(-1),
                px=px,
                py=py,
                pz=pz,
                values=values,
                title=metric.unit(unit).format('tex'),
                ncolors=user['nbins'],
                **compute_axis_limits(user),
            )
            plt.title(titles[step])
            strstep = f"{step:0{1+int(numpy.log10(ntimes))}d}"
            pngname = f"{name}-{strstep}.png"
            plt.savefig(tmpdir / pngname)
            plt.close()
            pngnames.append(pngname)
            if verbose:
                print(f"Completed step {step+1} / {ntimes}")
        except (IndexError, TypeError, ValueError) as exc:
            if user['on_error'] == 'abort':
                raise exc
            print(exception_message(exc))
            print("Continuing ...")
    return pngnames


def exception_message(exc: Exception):
    """Create a custom message from the given exception."""
    frames = reversed(traceback.extract_tb(exc.__traceback__))
    frame = next((f for f in frames if f.filename == __file__), None)
    message = f"Caught {type(exc).__qualname__}: {exc}"
    if frame is None:
        return message
    extra = f"(from {frame.filename}, line {frame.lineno}, in {frame.name})"
    return f"{message}\n{extra}"


def get_time_labels(times: eprem.physical.Vector):
    """Create an appropriate label for each time step."""
    if times.isunitless:
        return [f"step = {int(time)}" for time in times]
    return [
        f"time = {float(time)} [{times.unit.format('tex')}]"
        for time in times
    ]


def get_stream_indices(user: dict):
    """Compute the appropriate indices to subscript a stream quantity."""
    species = user['species']
    energy = user['energy']
    if species is None and energy is None:
        return (slice(None),)
    s = species or 0
    e = energy or [0]
    u = user['energy_unit']
    if len(energy) == 1:
        if u is None:
            return (slice(None), s, int(e[0]))
        return (slice(None), s, (float(e[0]), u))
    if len(energy) == 2:
        return (slice(None), s, (float(e[0]), e[1]))
    raise ValueError("Can only create a video at one energy value")


def get_stream_position(
    coordinate: str,
    streams: typing.Sequence[eprem.Stream],
    time: typing.Tuple[float, str],
    nshells: int,
) -> numpy.ndarray:
    """Compute Cartesian coordinates of the given streams."""
    array = numpy.zeros((len(streams), nshells))
    for i, stream in enumerate(streams):
        array[i] = stream[coordinate][time, :].withunit('au').squeezed
    return array


def get_stream_values(
    quantity: typing.Optional[str]=None,
    streams: typing.Optional[typing.Sequence[eprem.Stream]]=None,
    time: typing.Any=None,
    nshells: typing.Optional[int]=None,
    indices: typing.Optional[typing.Iterable]=None,
    unit: typing.Optional[str]=None,
) -> typing.Optional[numpy.ndarray]:
    """Compute numeric values of the named physical quantity."""
    if quantity is None:
        return
    nstreams = len(streams)
    values = numpy.zeros((nstreams, nshells))
    for i, stream in enumerate(streams):
        if quantity:
            tmp = stream[quantity]
            if unit:
                array = tmp.withunit(unit)
            else:
                array = tmp
            values[i] = array[time, *indices].squeezed
    return values.reshape(-1)


def compute_axis_limits(user: dict):
    """Compute axis limits for grid and data display."""
    grid_lims = user['grid_lims'] or (-2.0, +2.0)
    data_lims = user['data_lims'] or grid_lims
    groups = {'grid': grid_lims, 'data': data_lims}
    axes = ('x', 'y', 'z')
    axis_lims = {}
    for group, default in groups.items():
        for axis in axes:
            key = f'{group}_{axis}lim'
            axis_lims[key] = user[key] or default
    return axis_lims


def create_png(
    ckey: str,
    vmin: float,
    vmax: float,
    log: bool,
    sx: numpy.ndarray,
    sy: numpy.ndarray,
    sz: numpy.ndarray,
    px: numpy.ndarray,
    py: numpy.ndarray,
    pz: numpy.ndarray,
    values: typing.Optional[numpy.ndarray]=None,
    title: typing.Optional[str]=None,
    ncolors: typing.Optional[int]=None,
    data_xlim: typing.Optional[tuple[float, float]]=None,
    data_ylim: typing.Optional[tuple[float, float]]=None,
    data_zlim: typing.Optional[tuple[float, float]]=None,
    grid_xlim: typing.Optional[tuple[float, float]]=None,
    grid_ylim: typing.Optional[tuple[float, float]]=None,
    grid_zlim: typing.Optional[tuple[float, float]]=None,
) -> None:
    """Create a PNG image of EPREM stream and point observers."""
    fig = plt.figure(figsize=(14, 14))
    subplot = fig.add_subplot(111, projection='3d')
    nbins = ncolors or compute_nbins(vmin, vmax, log=log)
    colormap = mpl.colormaps[ckey].resampled(nbins)
    if log:
        tickfmt="%4.2e"
        cbins = numpy.logspace(numpy.log10(vmin), numpy.log10(vmax), nbins+1)
    else:
        tickfmt = "%g"
        cbins = numpy.linspace(vmin, vmax, nbins+1)
    norm = BoundaryNorm(cbins, nbins)
    cbar = fig.colorbar(
        ScalarMappable(norm=norm, cmap=colormap),
        ax=subplot,
        ticks=cbins,
        format=tickfmt,
    )
    opacity = numpy.logspace(-2, 0, nbins)
    cmap = colormap(numpy.arange(nbins), alpha=opacity)
    color = 'blue' if values is None else values
    inside = numpy.ones_like(values)
    if data_xlim:
        inside[(sx < data_xlim[0]) | (sx > data_xlim[1])] = 0.0
    if data_ylim:
        inside[(sy < data_ylim[0]) | (sy > data_ylim[1])] = 0.0
    if data_zlim:
        inside[(sz < data_zlim[0]) | (sz > data_zlim[1])] = 0.0
    subplot.scatter(
        sx, sy, sz,
        s=80*inside,
        c=color,
        cmap=ListedColormap(cmap),
        norm=norm,
        depthshade=False,
    )
    subplot.scatter(
        px, py, pz,
        s=100,
        c='k',
        depthshade=False,
    )
    if grid_xlim:
        subplot.set_xlim(*grid_xlim)
    if grid_ylim:
        subplot.set_ylim(*grid_ylim)
    if grid_zlim:
        subplot.set_zlim(*grid_zlim)
    subplot.set_xlabel('X [au]')
    subplot.set_ylabel('Y [au]')
    subplot.set_zlabel('Z [au]')
    cbar.set_label(title or '', rotation=270, fontsize=20, labelpad=30)


def compute_nbins(vmin: float, vmax: float, log: bool=False):
    """Compute an appropriate number of color bins."""
    v0 = numpy.log10(vmin) if log else vmin
    v1 = numpy.log10(vmax) if log else vmax
    return map_to_range(math.ceil(abs(v1 - v0)), 10, 20)


def map_to_range(x: int | float, x0: int, x1: int):
    """Map `x` to an integral value in `[x0, x1]`."""
    if (x0 <= x <= x1) and (isinstance(x, int) or x.is_integer()):
        return int(x)
    if x < 0:
        raise ValueError("Not valid for x < 0") from None
    if 0 < x < x0:
        return map_to_range(2*int(x), x0, x1)
    if x1 < x:
        for factor in range(x0, x1+1):
            if x % factor == 0:
                return map_to_range(factor, x0, x1)
    return x0


CLI_EPILOG = """
The argument to --time-step and --energy may be either 1) a single integer or 2)
a real number followed by an appropriate unit.

Note on axis limits: The axis-limit options allow the user to fine-tune the grid
and data display limits while providing sensible default values for each. The
value of --grid-lims will default to (-2.0, +2.0), the value of
--grid-{x,y,z}lim will default to the value of --grid-lims, the value of
--data-lims will default to the value of --grid-lims, and the value of
--data-{x,y,z}lim will default to the value of --data-lims.
"""
if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description=main.__doc__,
        epilog=CLI_EPILOG,
        formatter_class=argparse.RawTextHelpFormatter,
    )
    parser.add_argument(
        '--source',
        help="directory containing EPREM output",
        default=pathlib.Path().cwd(),
    )
    parser.add_argument(
        '--config',
        help="name of the EPREM configuration file",
    )
    parser.add_argument(
        '--quantity',
        help="physical quantity for observers to show",
    )
    parser.add_argument(
        '--unit',
        help="metric unit in which to show data",
    )
    parser.add_argument(
        '--time-step',
        help="cadence at which to visualize the given quantity",
        nargs='+',
        default=(1,),
        metavar=('TIME', 'UNIT'),
    )
    parser.add_argument(
        '--species',
        help="simulation species to show (if applicable)",
    )
    parser.add_argument(
        '--energy',
        help="energy at which to compute quantity (if applicable)",
        nargs='+',
        metavar=('ENERGY', 'UNIT'),
    )
    parser.add_argument(
        '--energy-unit',
        help="metric unit of ENERGY (default: MeV)",
    )
    parser.add_argument(
        '--colormap',
        help="a named Matplotlib colormap to use",
        default='viridis',
    )
    parser.add_argument(
        '--save-path',
        help="where to save the result",
    )
    parser.add_argument(
        '--save-pngs',
        help=(
            "save the individual PNG frames"
            " to DIR, if provided, or to a directory based on SAVE_PATH"
        ),
        nargs='?',
        default=False,
        metavar='DIR'
    )
    parser.add_argument(
        '--min',
        dest="cmin",
        help="minimum data value on color scale",
        type=float,
        metavar='MIN',
    )
    parser.add_argument(
        '--max',
        dest="cmax",
        help="maximum data value on color scale",
        type=float,
        metavar='MAX',
    )
    parser.add_argument(
        '--nbins',
        help="number of color bins between MIN and MAX",
        type=int,
    )
    parser.add_argument(
        '--log',
        help="plot values on a logarithmic scale",
        action='store_true',
    )
    parser.add_argument(
        '--grid-xlim',
        help="x-axis limits of the grid",
        type=float,
        nargs=2,
        metavar=('LO', 'HI'),
    )
    parser.add_argument(
        '--grid-ylim',
        help="y-axis limits of the grid",
        type=float,
        nargs=2,
        metavar=('LO', 'HI'),
    )
    parser.add_argument(
        '--grid-zlim',
        help="z-axis limits of the grid",
        type=float,
        nargs=2,
        metavar=('LO', 'HI'),
    )
    parser.add_argument(
        '--data-xlim',
        help="x-axis limits of the data",
        type=float,
        nargs=2,
        metavar=('LO', 'HI'),
    )
    parser.add_argument(
        '--data-ylim',
        help="y-axis limits of the data",
        type=float,
        nargs=2,
        metavar=('LO', 'HI'),
    )
    parser.add_argument(
        '--data-zlim',
        help="z-axis limits of the data",
        type=float,
        nargs=2,
        metavar=('LO', 'HI'),
    )
    parser.add_argument(
        '--grid-lims',
        help="common axis limits of the grid",
        type=float,
        nargs=2,
        metavar=('LO', 'HI'),
    )
    parser.add_argument(
        '--data-lims',
        help="common axis limits of the data",
        type=float,
        nargs=2,
        metavar=('LO', 'HI'),
    )
    parser.add_argument(
        '--fps',
        help="number of frames per second of result",
        type=float,
        default=3.0,
    )
    parser.add_argument(
        '--on-error',
        help="action to take when a known error type arises",
        choices={'continue', 'abort'},
    )
    parser.add_argument(
        '-v', '--verbose',
        help="print runtime messages",
        action='store_true',
    )
    args = parser.parse_args()
    main(**vars(args))
