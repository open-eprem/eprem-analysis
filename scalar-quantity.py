import argparse
import typing

import matplotlib.pyplot as plt

from eprempy.paths import fullpath
from support import interfaces
from support import plots


def main(
    quantity: str,
    num: typing.Optional[int]=None,
    source: typing.Optional[str]=None,
    config: typing.Optional[str]=None,
    outdir: typing.Optional[str]=None,
    verbose: bool=False,
    **user
) -> None:
    """Plot a vector quantity versus time or radius."""
    streams = interfaces.get_streams(source, config, num)
    plotdir = fullpath(outdir or source or '.')
    plotdir.mkdir(parents=True, exist_ok=True)
    for stream in streams:
        fig = plt.figure(layout='constrained')
        ax = fig.gca()
        plots.scalar_quantity(stream, user, quantity, axes=ax)
        if user['time'] is None:
            fig.suptitle(plots.make_title(stream, user, ['location']))
        elif user['location'] is None:
            fig.suptitle(plots.make_title(stream, user, ['time']))
        plotpath = plotdir / stream.source.with_suffix('.png').name
        if verbose:
            print(f"Saved {plotpath}")
        plt.savefig(plotpath)
        if user.get('show'):
            plt.show()
        plt.close()


epilog = """
Notes on time and location:
    * You must provide at least one time or one location. This routine will
      extract the corresponding slice of the named quantity along the other
      dimension.
    * Passing a metric unit as the final argument to --time will cause this
      routine to interpret the preceeding numerical values as physical times.
      Otherwise, this routine will interpret them as time-step indices.
    * Passing a metric unit as the final argument to --location will cause this
      routine to interpret the preceeding numerical values as physical radii.
      Otherwise, this routine will interpret them as shell indices.
"""
if __name__ == '__main__':
    parser = interfaces.Parser(
        description=main.__doc__,
        formatter_class=argparse.RawTextHelpFormatter,
        epilog=epilog,
        parents=[interfaces.common],
    )
    parser.add_argument(
        'quantity',
        help="physical quantity to plot",
    )
    parser.add_argument(
        '--unit',
        help="metric unit of the physical quantity",
    )
    parser.add_argument(
        '--time',
        help="time(s) at which to plot this quantity",
        nargs='*',
    )
    parser.add_argument(
        '--x-unit',
        help="metric unit of the x axis",
    )
    parser.add_argument(
        '--xlim',
        help="x-axis limits",
        nargs=2,
        type=float,
        metavar=('LO', 'HI'),
    )
    args = parser.parse_args()
    main(**vars(args))
