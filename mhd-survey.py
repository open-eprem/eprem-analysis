import argparse
import typing

import matplotlib.pyplot as plt

from eprempy import eprem
from eprempy.paths import fullpath
from support import interfaces
from support import plots


def main(
    source: typing.Optional[str]=None,
    config: typing.Optional[str]=None,
    outdir: typing.Optional[str]=None,
    verbose: bool=False,
    **user
) -> None:
    """Plot MHD quantities versus time."""
    observers = interfaces.get_observers(
        source=source,
        config=config,
        system='cgs',
        streams=user.get('stream'),
        points=user.get('point'),
    )
    plotdir = fullpath(outdir or source or '.')
    plotdir.mkdir(parents=True, exist_ok=True)
    for observer in observers.values():
        plot_observer(observer, user)
        if user['figname']:
            plotpath = plotdir / user['figname']
        else:
            plotpath = plotdir / observer.source.with_suffix('.png').name
        if verbose:
            print(f"Saved {plotpath}")
        plt.savefig(plotpath)
        if user.get('show'):
            plt.show()
        plt.close()


def plot_observer(observer: eprem.Observer, user: dict) -> None:
    """Create a survey plot for this observer."""
    fig, axs = plt.subplots(
        nrows=3,
        ncols=1,
        sharex=True,
        layout='constrained',
    )
    if isinstance(observer, eprem.Point) and user['time'] is None:
        user['location'] = 0
    for i, (k, v) in enumerate(CONTEXT.items()):
        user['unit'] = v['unit']
        user['yscale'] = v.get('yscale')
        v['f'](observer, user, k, axes=axs[i])
    if user['time'] is None:
        fig.suptitle(plots.make_title(observer, user, ['location']))
    elif user['location'] is None:
        fig.suptitle(plots.make_title(observer, user, ['time']))


CONTEXT = {
    'B': {
        'unit': 'nT',
        'f': plots.vector_quantity,
        'ylim': [-2, +2],
    },
    'U': {
        'unit': 'km / s',
        'f': plots.vector_quantity,
    },
    'rho': {
        'unit': 'cm^-3',
        'f': plots.scalar_quantity,
    },
}


epilog = """
  Notes on time and location
  --------------------------
    * You must provide at least one time or one location. This routine will
      extract the corresponding slice of the named quantity along the other
      dimension. The exception to this rule is when plotting point-observer
      data: if both time and location are missing, this routine will assume that
      you want all times at each point observer's fixed location.
    * Passing a metric unit as the final argument to --time will cause this
      routine to interpret the preceeding numerical values as physical times.
      Otherwise, this routine will interpret them as time-step indices.
    * Passing a metric unit as the final argument to --location will cause this
      routine to interpret the preceeding numerical values as physical radii.
      Otherwise, this routine will interpret them as shell indices.
"""
if __name__ == '__main__':
    parser = interfaces.Parser(
        description=main.__doc__,
        formatter_class=argparse.RawTextHelpFormatter,
        epilog=epilog,
        parents=[interfaces.common],
    )
    parser.add_argument(
        '--time',
        help="time(s) at which to plot this quantity",
        nargs='*',
    )
    parser.add_argument(
        '--x-unit',
        help="metric unit of the x axis",
    )
    parser.add_argument(
        '--xlim',
        help="x-axis limits",
        nargs=2,
        type=float,
        metavar=('LO', 'HI'),
    )
    parser.add_argument(
        '--figname',
        help="name (including extension) of the resultant figure",
    )
    args = parser.parse_args()
    main(**vars(args))
