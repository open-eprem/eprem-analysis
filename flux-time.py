import argparse
import typing

import matplotlib.pyplot as plt

from eprempy.paths import fullpath
from support import interfaces
from support import plots


def main(
    source: typing.Optional[str]=None,
    config: typing.Optional[str]=None,
    outdir: typing.Optional[str]=None,
    verbose: bool=False,
    **user
) -> None:
    """Create survey plots for one or more observers."""
    observers = interfaces.get_observers(
        source=source,
        config=config,
        system='cgs',
        streams=user.get('stream'),
        points=user.get('point'),
    )
    plotdir = fullpath(outdir or source or '.')
    plotdir.mkdir(parents=True, exist_ok=True)
    for observer in observers.values():
        fig = plt.figure(figsize=(10, 6), layout='constrained')
        ax = fig.gca()
        plots.flux_time(observer, user, axes=ax)
        title = plots.make_title(observer, user, ['location', 'species'])
        ax.set_title(title, fontsize=20)
        if user['figname']:
            plotpath = plotdir / user['figname']
        else:
            plotpath = plotdir / observer.source.with_suffix('.png').name
        if verbose:
            print(f"Saved {plotpath}")
        plt.savefig(plotpath)
        if user.get('show'):
            plt.show()
        plt.close()


epilog = """
The argument to --location may be one or more values followed by an optional
metric unit. If the unit is present, this routine will interpret the values as
radii. Otherwise, it will interpret the values as shell indices. The argument to
--energy behaves similarly.

You may pass an energy unit as the final argument to --energy or --energy-range,
or via --energy-unit. In case of a conflict, the argument to --energy-unit will
take precedence.
"""
if __name__ == '__main__':
    parser = interfaces.Parser(
        description=main.__doc__,
        formatter_class=argparse.RawTextHelpFormatter,
        epilog=epilog,
        parents=[interfaces.common],
    )
    parser.add_argument(
        '--time-range',
        help="lower and upper plot times, given in TIME_UNIT",
        nargs=2,
        type=float,
        metavar=('LO', 'HI'),
    )
    parser.add_argument(
        '--time-unit',
        help="metric unit in which to display times",
    )
    parser.add_argument(
        '--species',
        help="ion species to plot (symbol or index; default: 0)",
    )
    eg = parser.add_mutually_exclusive_group()
    eg.add_argument(
        '--energy',
        dest="energies",
        help="energy(-ies) at which to plot flux",
        nargs='*',
    )
    eg.add_argument(
        '--energy-range',
        help="range of energies at which to plot flux",
        nargs=3,
        type=float,
        metavar=('LO', 'HI', 'UNIT'),
    )
    parser.add_argument(
        '--energy-unit',
        help="metric unit in which to display energies (default: MeV)",
    )
    parser.add_argument(
        '--figname',
        help="name (including extension) of the resultant figure",
    )
    args = parser.parse_args()
    main(**vars(args))
