import math
import pathlib
import typing

import numpy
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.ticker as tck
from matplotlib.axes import Axes

from eprempy import eprem
from eprempy import measured
from eprempy import metric
from eprempy import physical
from eprempy import quantity
from . import interfaces


def vector_quantity(
    observer: eprem.Observer,
    user: dict,
    key: str,
    axes: typing.Optional[Axes]=None,
) -> None:
    """Plot components of a vector quantity for this stream."""
    if user['time'] is None and user['location'] is None:
        raise ValueError(
            f"Only one of time or location may be None"
        ) from None
    elif user['time'] is None and user['location'] is not None:
        vector_time(observer, user, key, axes=axes)
    elif user['location'] is None and user['time'] is not None:
        vector_radius(observer, user, key, axes=axes)
    else:
        raise ValueError(
            f"One of either time or location must be None"
        ) from None


def vector_radius(
    observer: eprem.Observer,
    user: dict,
    key: str,
    axes: typing.Optional[Axes]=None,
) -> None:
    """Plot components of a vector quantity versus radius for this stream."""
    ax = axes or plt.gca()
    time = interfaces.get_time(user)
    x = observer['radius'][time, :].withunit(user['x_unit'] or 'au')
    for c, label in COMPONENTS.items():
        q = f'{key}{c}'
        y = observer[q][time, :]
        unit = user['unit'] or y.unit
        ax.plot(x.squeezed, y.withunit(unit).squeezed, label=label)
    ax.set_xlabel(rf"Radius [{x.unit.format('tex')}]", fontsize=14)
    ax.set_ylabel(
        rf"{LABELS[key]}$_{{r,\theta,\phi}}$ [{y.unit.format('tex')}]"
    )
    ax.set_yscale(user.get('yscale') or 'linear')
    if user.get('xlim'):
        ax.set_xlim(user['xlim'])
    if user.get('ylim'):
        ax.set_ylim(user['ylim'])
    ax.legend()
    ax.label_outer()


def vector_time(
    observer: eprem.Observer,
    user: dict,
    key: str,
    axes: typing.Optional[Axes]=None,
) -> None:
    """Plot components of a vector quantity versus time for this stream."""
    ax = axes or plt.gca()
    if isinstance(observer, eprem.Point):
        location = 0
    else:
        location = interfaces.get_location(user, observer)
    x = observer.times.withunit(user['x_unit'] or 'hour')
    for c, label in COMPONENTS.items():
        q = f'{key}{c}'
        y = observer[q][:, location]
        unit = metric.unit(user['unit']) or y.unit
        ax.plot(x, y.withunit(unit).squeezed, label=label)
    ax.set_xlabel(rf"Time [{x.unit.format('tex')}]", fontsize=14)
    ax.set_ylabel(
        rf"{LABELS[key]}$_{{r,\theta,\phi}}$ [{unit.format('tex')}]"
    )
    ax.set_yscale(user.get('yscale') or 'linear')
    if user.get('xlim'):
        ax.set_xlim(user['xlim'])
    if user.get('ylim'):
        ax.set_ylim(user['ylim'])
    ax.legend()
    ticks = get_time_ticks(str(x.unit))
    ax.xaxis.set_major_locator(ticks['major'])
    ax.xaxis.set_minor_locator(ticks['minor'])
    ax.label_outer()


def scalar_quantity(
    observer: eprem.Observer,
    user: dict,
    key: str,
    axes: typing.Optional[Axes]=None,
) -> None:
    """Plot a scalar quantity for this stream."""
    if user['time'] is None and user['location'] is None:
        raise ValueError(
            f"Only one of time or location may be None"
        ) from None
    elif user['time'] is None and user['location'] is not None:
        scalar_time(observer, user, key, axes=axes)
    elif user['location'] is None and user['time'] is not None:
        scalar_radius(observer, user, key, axes=axes)
    else:
        raise ValueError(
            f"One of either time or location must be None"
        ) from None


def scalar_radius(
    observer: eprem.Observer,
    user: dict,
    key: str,
    axes: typing.Optional[Axes]=None,
) -> None:
    """Plot a scalar quantity versus radius for this stream."""
    ax = axes or plt.gca()
    time = interfaces.get_time(user)
    x = observer['radius'][time, :].withunit(user['x_unit'] or 'au')
    y = observer[key][time, :]
    unit = user['unit'] or y.unit
    ax.plot(x.squeezed, y.withunit(unit).squeezed)
    ax.set_xlabel(rf"Radius [{x.unit.format('tex')}]", fontsize=14)
    ax.set_ylabel(rf"{LABELS[key]} [{y.unit.format('tex')}]")
    ax.set_yscale(user.get('yscale') or 'linear')
    if user.get('xlim'):
        ax.set_xlim(user['xlim'])
    if user.get('ylim'):
        ax.set_ylim(user['ylim'])
    ax.label_outer()


def scalar_time(
    observer: eprem.Observer,
    user: dict,
    key: str,
    axes: typing.Optional[Axes]=None,
) -> None:
    """Plot a scalar quantity versus time for this stream."""
    ax = axes or plt.gca()
    if isinstance(observer, eprem.Point):
        location = 0
    else:
        location = interfaces.get_location(user, observer)
    x = observer.times.withunit(user['x_unit'] or 'hour')
    y = observer[key][:, location].withunit(user['unit'])
    unit = user['unit'] or y.unit
    ax.plot(x, y.withunit(unit).squeezed)
    ax.set_xlabel(rf"Time [{x.unit.format('tex')}]", fontsize=14)
    ax.set_ylabel(rf"{LABELS[key]} [{y.unit.format('tex')}]")
    ax.set_yscale(user.get('yscale') or 'linear')
    if user.get('xlim'):
        ax.set_xlim(user['xlim'])
    if user.get('ylim'):
        ax.set_ylim(user['ylim'])
    ticks = get_time_ticks(str(x.unit))
    ax.xaxis.set_major_locator(ticks['major'])
    ax.xaxis.set_minor_locator(ticks['minor'])
    ax.label_outer()


def flux_spectrogram(
    observer: eprem.Observer,
    user: dict,
    axes: typing.Optional[Axes]=None,
) -> None:
    """Plot flux as a function of time and energy."""
    if isinstance(observer, eprem.Point):
        location = 0
    else:
        location = interfaces.get_location(user, observer)
    species = interfaces.get_species(user)
    units = interfaces.get_units(user)
    times = observer.times.withunit(units['time'])
    energies = observer.energies.withunit(units['energy'])
    flux = observer['flux'].withunit(units['flux'])
    ax = axes or plt.gca()
    xx, yy = numpy.meshgrid(times.data, energies.data, indexing='ij')
    pcm = ax.pcolormesh(
        xx, yy,
        flux[:, location, species, :].squeezed,
        norm='log',
        vmin=1e-30,
        vmax=1e10,
        shading='nearest',
    )
    ax.set_xscale('linear')
    ax.set_yscale('log')
    ax.set_xlabel(f"Time [{times.unit}]", fontsize=14)
    ax.set_ylabel(f"Energy [{energies.unit}]", fontsize=14)
    fig = plt.gcf()
    fig.colorbar(pcm, ax=ax)


def flux_time(
    observer: eprem.Observer,
    user: dict,
    axes: typing.Optional[Axes]=None,
) -> None:
    """Plot flux versus time for this stream."""
    location = interfaces.get_location(user, observer)
    species = interfaces.get_species(user)
    units = interfaces.get_units(user)
    energies = interfaces.get_energies(user, observer)
    flux = observer['flux'].withunit(units['flux'])
    if user.get('time_range'):
        ttmp = observer.times.withunit(units['time'])
        it0, itf = ttmp.index(*user['time_range'])
        times = ttmp[it0:itf]
    else:
        times = observer.times.withunit(units['time'])
        it0, itf = 0, len(times)
    cmap = mpl.colormaps['jet']
    colors = cmap(numpy.linspace(0, 1, len(energies)))
    ax = axes or plt.gca()
    for i, energy in enumerate(energies):
        array = flux[it0:itf, location, species, energy].squeezed
        label = f"{float(energy):.3f} {energies.unit}"
        ax.plot(times, array, label=label, color=colors[i])
    if user.get('ylim'):
        ax.set_ylim(user['ylim'])
    ax.set_xlabel(f"Time [{times.unit}]", fontsize=14)
    ax.set_ylabel(fr"Flux [{flux.unit.format('tex')}]", fontsize=14)
    ax.set_xscale('linear')
    ax.set_yscale('log')
    ax.legend(
        loc='center left',
        bbox_to_anchor=(1.0, 0.5),
        handlelength=1.0,
        ncols=math.ceil(len(energies) / 20),
    )
    ticks = get_time_ticks(str(times.unit))
    ax.xaxis.set_major_locator(ticks['major'])
    ax.xaxis.set_minor_locator(ticks['minor'])


def flux_energy(
    observer: eprem.Observer,
    user: dict,
    axes: typing.Optional[Axes]=None,
) -> None:
    """Create a plot of flux versus energy for this stream."""
    times = interfaces.get_times(user)
    locations = interfaces.get_locations(user)
    species = interfaces.get_species(user)
    units = interfaces.get_units(user)
    flux = observer['flux'].withunit(units['flux'])
    energies = observer.energies.withunit(units['energy'])
    ax = axes or plt.gca()
    cmap = mpl.colormaps['jet']
    colors = cmap(numpy.linspace(0, 1, len(times)))
    legend = False
    if len(times) > 1 and len(locations) > 1:
        raise ValueError
    elif len(times) == 1 and len(locations) > 1:
        colors = cmap(numpy.linspace(0, 1, len(locations)))
        for i, location in enumerate(locations):
            array = flux[times[0], location, species, :].squeezed
            if isinstance(locations, measured.Object):
                label = f"r = {float(location):.3f} {locations.unit}"
            else:
                label = f"shell = {int(location)}"
            ax.plot(energies, array, label=label, color=colors[i])
        ax.set_title(make_title(observer, user, ['time', 'species']))
        legend = True
    elif len(times) > 1 and len(locations) == 1:
        colors = cmap(numpy.linspace(0, 1, len(times)))
        for i, time in enumerate(times):
            array = flux[time, locations[0], species, :].squeezed
            if isinstance(times, measured.Object):
                label = f"t = {float(time):.1f} {times.unit}"
            else:
                label = f"time step {int(time)}"
            ax.plot(energies, array, label=label, color=colors[i])
        ax.set_title(make_title(observer, user, ['location', 'species']))
        legend = True
    else:
        array = flux[times[0], locations[0], species, :].squeezed
        ax.plot(energies, array)
        title = make_title(observer, user, ['time', 'location', 'species'])
        ax.set_title(title)
    if user.get('ylim'):
        ax.set_ylim(user['ylim'])
    ax.set_xlabel(f"Energy [{energies.unit}]", fontsize=14)
    ax.set_ylabel(fr"Flux [{flux.unit.format('tex')}]", fontsize=14)
    ax.set_xscale('log')
    ax.set_yscale('log')
    if legend:
        ax.legend(
            loc='center left',
            bbox_to_anchor=(1.0, 0.5),
            handlelength=1.0,
        )


def fluence_energy(
    observer: eprem.Observer,
    user: dict,
    axes: typing.Optional[Axes]=None,
) -> None:
    """Create a plot of fluence versus energy for this stream."""
    location = interfaces.get_location(user, observer)
    species = interfaces.get_species(user)
    units = interfaces.get_units(user)
    fluence = observer['fluence'].withunit(units['fluence'])
    energies = observer.energies.withunit(units['energy'])
    array = fluence[-1, location, species, :].squeezed
    ax = axes or plt.gca()
    ax.plot(energies, array)
    if user.get('ylim'):
        ax.set_ylim(user['ylim'])
    ax.set_xlabel(f"Energy [{energies.unit}]", fontsize=14)
    ax.set_ylabel(fr"Fluence [{fluence.unit.format('tex')}]", fontsize=14)
    ax.set_xscale('log')
    ax.set_yscale('log')


def intflux_time(
    observer: eprem.Observer,
    user: dict,
    axes: typing.Optional[Axes]=None,
) -> None:
    """Create a plot of integral flux versus time for this stream."""
    location = interfaces.get_location(user, observer)
    species = interfaces.get_species(user)
    units = interfaces.get_units(user)
    intflux = observer['integral flux'].withunit(units['integral flux'])
    if user.get('energies'):
        energies = quantity.measure(*user['energies'])
    else:
        energies = quantity.measure(10.0, 50.0, 100.0, units['energy'])
    times = observer.times.withunit(units['time'])
    ax = axes or plt.gca()
    for energy in energies:
        array = intflux[:, location, species, energy].squeezed
        label = fr"$\geq${float(energy)} {energies.unit}"
        ax.plot(times, array, label=label)
    if user.get('ylim'):
        ax.set_ylim(user['ylim'])
    ax.set_xlabel(f"Time [{times.unit}]", fontsize=14)
    ax.set_ylabel(fr"Integral Flux [{intflux.unit.format('tex')}]", fontsize=14)
    ax.set_xscale('linear')
    ax.set_yscale('log')
    ax.legend(loc='center left', bbox_to_anchor=(1.0, 0.5), handlelength=1.0)
    ticks = get_time_ticks(str(times.unit))
    ax.xaxis.set_major_locator(ticks['major'])
    ax.xaxis.set_minor_locator(ticks['minor'])


def get_time_ticks(unit: str):
    """Define appropriate tick locations for a time unit."""
    if unit in {'s', 'second'}:
        return {
            'major': tck.MultipleLocator(3600.0),
            'minor': tck.MultipleLocator(600.0),
        }
    if unit in {'minute'}:
        return {
            'major': tck.MultipleLocator(60.0),
            'minor': tck.MultipleLocator(10.0),
        }
    if unit in {'h', 'hour'}:
        return {
            'major': tck.MultipleLocator(24.0),
            'minor': tck.MultipleLocator(6.0),
        }
    if unit in {'d', 'day'}:
        return {
            'major': tck.MultipleLocator(1.0),
            'minor': tck.MultipleLocator(0.25),
        }
    raise ValueError(unit)


def compute_yloglim(maxval):
    """Compute logarithmic y-axis limits based on `maxval`."""
    ylogmax = int(numpy.log10(maxval)) + 1
    return 10**(ylogmax-6), 10**ylogmax


_MEASURED_TYPES = (
    quantity.Measurement,
    measured.Value,
    physical.Scalar,
)


def make_title(
    observer: eprem.Observer,
    user: dict,
    keys: typing.Iterable[str],
) -> str:
    """Create a title string for plots."""
    parts = []
    if 'time' in keys and 'time' in user:
        time = interfaces.get_time(user)
        if isinstance(time, _MEASURED_TYPES):
            tmpstr = f"time = {float(time):.2f} {time.unit}"
        elif isinstance(time, int):
            tmpstr = f"time step {time}"
        else:
            TypeError(time)
        parts.append(tmpstr)
    if 'location' in keys and 'location' in user:
        if isinstance(observer, eprem.Point):
            location = observer.r.withunit('au')
        else:
            location = interfaces.get_location(user, observer)
        if isinstance(location, _MEASURED_TYPES):
            tmpstr = f"radius = {float(location):.2f} {location.unit}"
        elif isinstance(location, int):
            tmpstr = f"shell = {location}"
        else:
            raise TypeError(location)
        parts.append(tmpstr)
    if 'species' in keys and 'species' in user:
        species = interfaces.get_species(user)
        if isinstance(species, int):
            tmpstr = f"species = {observer.species.data[species]}"
        elif isinstance(species, str):
            tmpstr = f"species = {species}"
        else:
            raise TypeError(species)
        parts.append(tmpstr)
    return ' | '.join(parts)


def get_plotpath(user: dict, plotdir: pathlib.Path, observer: eprem.Observer):
    """Compute the appropriate path for the plot."""
    if user['figname']:
        return plotdir / user['figname']
    return plotdir / observer.source.with_suffix('.png').name


COMPONENTS = {
    'r': r'$r$',
    'theta': r'$\theta$',
    'phi': r'$\phi$',
}


LABELS = {
    'B': r'$B$',
    'Br': r'$B_r$',
    'Btheta': r'$B_\theta$',
    'Bphi': r'$B_\phi$',
    'U': r'$U$',
    'Ur': r'$U_r$',
    'Utheta': r'$U_\theta$',
    'Uphi': r'$U_\phi$',
    'rho': r'$\rho$',
}

